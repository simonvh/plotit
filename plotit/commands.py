#!/usr/bin/env python
import sys
import matplotlib.pyplot as plt
from plotit.color import parse_colors
from plotit.io import read_file
import seaborn as sns
import numpy as np

def line(args):
    infile = args.infile
    outfile = args.outfile
    colors = parse_colors(args.colors)
    
    xcol = int(args.xcol) - 1 
    ycol = [int(y) - 1 for y in args.ycol.split(",")]
    
    names = []
    if args.names:
        names = args.names.split(",")

    data = read_file(infile)
    
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    for i,y in enumerate(ycol):
        plt.plot(data[xcol,:], data[y,:], color=colors[i % len(colors)])
    
    if args.xlim:
        xlim = [float(x) for x in args.xlim.split(",")]
        ax.set_xlim(xlim)
    if args.ylim:
        ylim = [float(y) for y in args.ylim.split(",")]
        ax.set_ylim(ylim)

    plt.savefig(outfile)

def scatter(args):
    infile = args.infile
    outfile = args.outfile
    colors = parse_colors(args.colors)
    
    xcol = int(args.xcol) - 1 
    ycol = [int(y) - 1 for y in args.ycol.split(",")]
    
    names = []
    if args.names:
        names = args.names.split(",")

    data = read_file(infile)

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    for i,y in enumerate(ycol):
        if args.cloud:
            xv = np.sort(np.unique(data[xcol,:]))
            md = np.mean(xv[1:] - xv[:-1]) 
            offsets = np.random.randn(len(data[xcol,:])) * (0.075 * md)
        else:
            offsets = np.zeros(len(data[xcol,:]))
        plt.scatter(data[xcol,:] + offsets, data[y,:], color=colors[i % len(colors)])
    
    if args.xlim:
        xlim = [float(x) for x in args.xlim.split(",")]
        ax.set_xlim(xlim)
    if args.ylim:
        ylim = [float(y) for y in args.ylim.split(",")]
        ax.set_ylim(ylim)
    
    plt.savefig(outfile)


