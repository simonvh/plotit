#!/usr/bin/env python
import sys
import matplotlib.pyplot as plt
from plotit.color import parse_colors
import seaborn as sns
import numpy as np

def guess_names(infile):
    with open(infile) as f:
        line = f.readline()
        vals = line.strip().split("\t")
        ncol = len(vals)
        for x in range(ncol):
            data.append([])
    
        try:
            for i,v in enumerate(vals):
                v = float(v)
                data[i].append(v)
        except:
            if len(names) == 0:
                sys.stderr.write("Assuming first row is header...\n")
                names = vals
 
def read_file(infile):
    data = []
    with open(infile) as f:
        line = f.readline()
        vals = line.strip().split("\t")
        ncol = len(vals)
        for x in range(ncol):
            data.append([])
    
        try:
            for i,v in enumerate(vals):
                v = float(v)
                data[i].append(v)
        except:
            pass

        line = f.readline()
        while line:
            vals = line.strip().split("\t")
            for i,v in enumerate(vals):
                v = float(v)
                data[i].append(v)
            line = f.readline()
    
    return np.array(data)
