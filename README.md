# Description

`plotit` is a simple command line utility to plot tab-separated files from the command line. Many output formats are supported (svg, pdf, ps, png, jpg, etc.), depending on the matplotlib backend.

# Prerequisites

Python modules:

* numpy
* matplotlib
* pandas
* seaborn
* colorbrewer

# Installation

    sudo python setup.py install

You might have to set the matplotlib backend in `/usr/lib64/python2.7/site-packages/matplotlib/mpl-data/matplotlibrc` or `~/.matplotlibrc`. See the [matplotlib documentation](http://matplotlib.org/users/customizing.html).