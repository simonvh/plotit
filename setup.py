from distutils.core import setup
from setuptools.command.test import test as TestCommand
import sys

VERSION = "1.00"
DESCRIPTION = """
plotit - simple command line plotter
"""
class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)

setup (name = 'plotit',
        version = VERSION,
        description = DESCRIPTION,
        author='Simon van Heeringen',
        author_email='simon.vanheeringen@gmail.com',
        license='MIT',
        packages=[
            'plotit'
        ],
        scripts=[
            "scripts/plotit",
        ],
        data_files=[],
        tests_require=['pytest'],
        install_requires=[
                        "numpy",
                        "pandas",
                        "matplotlib",
                        "seaborn",
                        "colorbrewer",
                        ],
        cmdclass = {'test': PyTest},
)
